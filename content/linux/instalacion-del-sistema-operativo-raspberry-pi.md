+++
date = 2022-05-01
title = "Instalación del sistema operativo Raspberry Pi"
description = "Creando pequeños servidores Linux para casa"
authors = ["Jorge Medina"]
categories = [
    "infraestructura",
    "linux",
    "raspberry",
]
+++

Saludos a todos los lectores, ya hace algunos años que no piso un centro de datos y que no he instalado servidores
físicos, ya saben, pura nube, y dado que por home office he migrado mi laboratorio de servidores de arquitectura
x86 a ARM, les compartiré mi experiencia con la instalación de servidores Linux caseros con las Raspberry Pi, esta
vez compartiré algo más básico, algo que sirva para empezar un proyecto de servidor de casa, esto quiere decir,
que empezaremos por enseñar la instalación del sistema operativo Raspberry Pi, el cual está basado en Debian,
usaremos este sistema pera instalar nuestros pequeños servidores linux, espero que les sea de utilidad.

## Introducción

Esta simple guía te ayudara a instalar el sistema operativo Raspberry PI en una máquina Raspberry PI 3 o 4, te voy
a mostrar como hacer las tareas de configuración básicas, como la configuración de la red, cambiar la contraseña,
actualizar el sistema e instalar paquetes y otras tareas. Además realizaremos tareas para desactivar los servicios
que vienen pre instalados en la distribución, esto para hacer un uso más eficiente del sistema, y también más
sencillo el mantenimiento.

## Requisitos

Además de contar con una Raspberry PI 3 o 4 con su adaptador de corriendo soportado, para la configuración básica
requerimos lo siguiente:

*  Tarjeta SD de 32 GB
*  Sistema Operativo Raspberry PI Lite (Legacy), de 32-bit con Debian 10 buster
*  Monitor con conexión HDMI o adaptador VGA
*  Teclado USB
*  Conexión Ethernet
*  Dirección IP Privada
*  Conexión a Internet

## Instalación

Para la instalación inicial voy a usar un sistema linux, necesitamos suficiente espacio en disco para descargar la
imagen del sistema operativo raspberry pi y un lector de tarjetas SD. Descargamos la imagen oficial desde la página
siguiente:

* [Raspberry PI OS Download page](https://www.raspberrypi.com/software/operating-systems/)

Extraemos el archivo de la imagen:

```shell
$ cd Downloads
$ ls *.xz
2022-04-04-raspios-buster-armhf-lite.img.xz
$ xz -d -v 2022-04-04-raspios-buster-armhf-lite.img.xz
$ ls *.img
2022-04-04-raspios-buster-armhf-lite.img
```

Descargamos **Raspberry PI Imager** desde la siguiente liga:

* [Raspberry PI Imager](https://www.raspberrypi.com/software/)

Quemamos la imagen en una tarjeta SD y después debemos realizar lo siguiente:

* Instalar la tarjeta SD en la raspberry Pi
* Conectar el cable HDMI al monitor
* Conectar el cable Ethernet a la red local
* Conectar el adaptador de corriente
* Esperar a que el sistema arranque

## Cambiar la contraseña

Por razones de seguridad, debemos cambiar la contraseña predeterminada del usuario **pi**, hacemos login localmente
en el sistema, veremos algo así en pantalla:

```shell
Rasbpian GNU/Linux 10 raspberrypi tty1
raspberrypi login: 
```

El usuario predeterminado es **pi** y **raspberry** su contraseña, ahora corremos **raspi-config** para actualizar la contraseña:

```shell
sudo raspi-config
```

Vamos al menú **1 System Options**, seleccionamos **S3 Password** y lo cambiamos.

## Expandir el sistema de archivos

El siguiente paso es expandir el sistema de archivos siguiendo los siguientes pasos:

```shell
sudo raspi-config
```

Vamos a el menú **Advanced Options** seleccionamos **A Expand Filesystem**, esperamos a que termine la operación
para terminar y salir de la herramienta, después reiniciamos:

```shell
sudo reboot
```

## Configuración de Red

Ahora podemos cambiar el nombre del host usando la herramienta **raspi-config**:

```shell
sudo raspi-config
```

Vamos al menú **1 System Options** seleccionamos **S4 Hostname**, escribimos el nombre del host y seleccionamos **OK**.

Configuramos la conexión Ethernet para la raspberry pi, edita el archivo de configuración del cliente DHCP:

```shell
sudo vim /etc/dhcpcd.conf
```

Al final del archivo agregamos las siguientes líneas para configurar la dirección IP privada:

```shell
# LAN Interface
interface eth0
static ip_address=10.101.100.250/24
static routers=10.101.100.1
static domain_name_servers=8.8.8.8
```

Guardamos el archivo y reiniciamos el sistema:

```shell
sudo reboot
```

## Actualización del sistema

Ahora que el sistema tiene conectividad local e Internet, actualizamos la lista de paquetes de APT y actualizamos
el sistema:

```shell
sudo apt update
sudo apt upgrade
```

Después, reiniciamos la máquina:

```shell
sudo reboot
```

Esperamos, y después iniciamos sesión para realizar las siguientes tareas.

## Configuración SSH

Para administrar el sistema de forma remota a través de la red, habilita el servicio SSH: 

```shell
sudo raspi-config
```

Vamos al menú **3 Interface** Options seleccionamos **P2 SSH**, confirmamos y finalizamos.

Opcionalmente puede instalar la llave SSH pública en el archivo de llaves autorizadas, por ejemplo: 

```shell
mkdir .ssh
chmod 700
vim .ssh/authorized_keys
chmod 600
```

## Instalación de Paquetes

Instalemos algunos paquetes para administrar el sistema:

```shell
sudo apt install vim screen multitail htop
```

Ahora unas herramientas para administración de la red:

```shell
sudo apt curl wget iftop tcpdump tshark nmap
```

Limpiamos los paquetes:

```shell
sudo apt clean
sudo apt autoclean
```

## Respaldo de la tarjeta SD

Después de que haz hecho todas las tareas previas, se recomienda hacer una copia de seguridad de el contenido de
la tarjeta SD, así podrás tener u n respaldo en caso de que falle la tarjeta o en caso de que quieras instalar el
sistema de nuevo.

En linux usa **fdisk** para obtener el nombre del dispositivo de la tarjeta SD:

```shell
sudo fdisk -l
```

En macos usa **diskutil** para obtener el nombre del dispositivo:

```shell
$ sudo diskutil list
...
/dev/disk5 (external, physical):
 #:     TYPE NAME                    SIZE       IDENTIFIER
 0:     FDisk_partition_scheme      *15.5 GB    disk5
 1:     Windows_FAT_32 boot          268.4 MB   disk5s1
 2:     Linux                        15.2 GB    disk5s2
```

El nombre del dispositivo es **/dev/disk5**.

```shell
sudo dd if=/dev/disk5 of=raspberry_pi_buster_base.img bs=4M 
```

Esperamos a que la operación termine y al final expulsamos la tarjeta SD.

## Restaurando la imagen base

En caso de que nos falle la tarjeta SD, lo cual es común con memorias de baja calidad o cuando les hacemos un uso
muy intensivo en nuestras instalaciones en caso de que solo queramos reinstalar a un punto conocido para realizar
más prácticas, o quizás queremos instalar más. raspberry Pi en base a esta imagen base. Para no tener que realizar
todos los pasos descritos en esta guía, es mejor empezar desde un estado conocido, por lo tanto se recomienda usar
Raspberry Pi Imager para hacer una instalación personalizada, busca el archivo de la imagen base raspberry_pi
buster_base.img, selecciona el destino, y listo, en unos minutos tienes reinstalado tu sistema, o tu granja de
pequeños servidores Raspberry Pi.

## Desactivando WIFI y Bluetooth

Ya que nuestros servidores solo tendrán comunicación por cable Ethernet, vamos a desactivar los dispositivos WIFI y
Bluetooth ya que no los usaremos, para esto editamos el archivo **/boot/config.txt**:

```shell
sudo vi /boot/config.txt
```

Al final agregamos las siguiente líneas:

```shell
# Disable wifi and bluetooth
dtoverlay=disable-wifi
dtoverlay=disable-bt
```

Desactivamos los servicios permanentemente:

```shell
sudo systemctl disable hciuart.service
sudo systemctl disable bluealsa.service
sudo systemctl disable bluetooth.service 
```

## Desactivamos servicios sin usar

Desactivamos el demonio avahi:

```shell
sudo systemctl stop avahi-daemon
sudo systemctl disable avahi-daemon 
```

Desactivamos triggerhappy:

```shell
sudo systemctl stop triggerhappy
sudo systemctl disable triggerhappy 
```

Desactivamos wpa supplicant:

```shell
sudo systemctl stop wpa_supplicant
sudo systemctl disable wpa_supplicant
```

Al final de desactivar todos los dispositivos y recursos que no usaremos en nuestro servidor, el uso de recursos
se debería ver algo así:

![Raspberry Pi shell htop](/posts/instalacion-del-sistema-operativo-raspberry-pi/raspberry-pi-shell-htop.png)

Como se puede ver el uso de CPU y RAM es muy poco, aun se pueden seguir haciendo optimizaciones, pero esto va a
depender del caso de uso, para una imagen inicial no esta mal.

Desactivar:

* dbus-daemon
* NetworkManager

Saludos a todos.

## Referencias

Dejamos las siguientes referencias externas para complementar la documentación:

* [Raspberry Pi](https://www.raspberrypi.com/)
* [Raspberry PI Software](https://www.raspberrypi.com/software/)
* [Raspberry PI Documentation](https://www.raspberrypi.com/documentation/)
