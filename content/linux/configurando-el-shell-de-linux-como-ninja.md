+++
date = 2020-04-30
title = "Configurando el shell de Linux como ninja"
description = "Tunning de bash para perezosos"
authors = ["Jorge Medina"]
categories = [
    "linux",
    "shell scripts",
]
+++

Que tal a todos, buen día donde sea que se encuentren. Durante los últimos días he traído en mente enseñar como
hacer un buen flujo de trabajo en shell al trabajar en un sistema GNU/Linux. Pongo aquí mi experiencia la cual
espero pueda aportarte algo, donde la mayor parte del trabajo lo hacemos desde nuestro computador de escritorio
(o laptop) y que hemos elegido el shell bash.

Los objetivos son:

* Conoce tu shell. ¿Cual shell? ¿Como se ejecuta?
* Como configuro auto completado, prompt, historial, alias
* Mostrar algunos atajos del teclado
* Usando un emulador de terminal

![Flash ninja](/posts/configurando-el-shell-de-linux-como-ninja/flash-ninja.png)

## Conoce tu shell

En la mayoría de distribuciones se usa por default el shell bash. Esto significa que el usuario no root que se
crea en la instalación (o posteriormente) se le asigna el shell bash, y esto lo podemos comprobar consultando
el archivo **passwd(5)**, por ejemplo:

```shell
$ grep $(whoami) /etc/passwd
kronops:x:1000:1000:Jorge Medina,,,:/home/kronops:/bin/bash
```

¿Como compruebo que estoy usando el shell bash? Podrías consultar el contenido de la variable de ambiente **SHELL**
o también revisar el proceso primario de la sesión:

```shell
$ echo $SHELL
/bin/bash
```

```shell
$ ps -p $$
  PID TTY          TIME CMD
22546 pts/5    00:00:00 bash
```

Veamos como se tiene parametrizado el shell por default usando el comando set:

```shell
$ set -o
allexport       off
braceexpand     on
emacs           on
errexit         off
errtrace        off
functrace       off
hashall         on
histexpand      on
history         on
ignoreeof       off
interactive-comments        on
keyword         off
monitor         on
noclobber       off
noexec          off
noglob          off
nolog           off
notify          off
nounset         off
onecmd          off
physical        off
pipefail        off
posix           off
privileged      off
verbose         off
vi              off
xtrace          off
```

En la salida de arriba vemos varias funcionalidades de bash y su valor actual, por ejemplo que tiene el modo emacs
activado y el modo vi off :P.

Ahora que ya sabemos que estamos en bash, conozcamos cuales son las fuentes de conocimiento para este programa que
usaremos casi todos los días en esta vida como sysadmin, desarrollador o curioso en general :).

Veamos que documentación hay disponible en el sistema:

```shell
$ whatis bash
bash (1)             - GNU Bourne-Again SHell
```

Podemos consultar la documentación de usuario en la sección 1 del manual de bash, para verlo usamos:

```shell
man 1 bash
```

En el manual encontraremos información sobre uso como:

* Invocación
* Definiciones de meta caracteres y operadores de control.
* Palabras reservadas.
* La gramática del shell, como comandos simples, pipelines, listas y comandos compuestos, funciones de shell.
* Uso de comentarios, encomillados, parametrización, expansiones, redirecciones, alias.
* Y mucho más.

## Personaliza el shell

En esta sección veremos como personalizar el shell y entenderemos los métodos oficiales para configurar el shell
bash para un usuario.

En los sistemas Linux cuando usamos bash en modo login el primer archivo que se lee en tiempo de ejecución es
**/etc/profile**. Veamos su contenido:

```shell
vim /etc/profile
```

![Bash /etc/profile](/posts/configurando-el-shell-de-linux-como-ninja/bash-etc-profile.png)

Este script se usa para definir el prompt del shell. Si existe el archivo **/etc/bash.bashrc** lo incluye y carga
los parámetros ahí definidos.

Lo que se muestra arriba es la configuración global del sistema y aplica para todos los usuarios cuando inician
sesión. Si queremos personalizar el shell de un usuario en particular, se deben personalizar sus archivos dot o
_dotfiles_, que son archivos ocultos almacenanados en el _$HOME_ de usuario. Los principales son:

```shell
vim .profile
```

![Bash .profile](/posts/configurando-el-shell-de-linux-como-ninja/bash-dot-profile.png)

En este caso incluyo la lectura del archivo **$HOME/.bashrc** y despues defino diferentes rutas en el **PATH**.
En este archivo defino varias cosas por default y se crea desde el esqueleto al crear el primer usuario, yo solo
le agregué la última ruta a snap.

Veamos la personalización local que uso en mis equipos personales:

```shell
vim .bash_local
```

![Bash local](/posts/configurando-el-shell-de-linux-como-ninja/bash-local.png)

Esto es lo que se hace aquí:

* Configuraciones de historial más grande y formateado
* **Manejo de lineas y columnas en la salida del shell
* Activa auto completado de comandos
* Terminal con colores
* Configuración de prompt de dos líneas
* Uso de colores en la salida de grep
* Uso del editor vim como editor predeterminado
* Alias personalizados en **$HOME/.bash_aliases**
* Configuraciones locales propias en **$HOME/.bash_local**

Veamos como configuro los alias en $HOME/.bash_aliases:

```shell
vim .bash_aliases 
```

![Bash aliases](/posts/configurando-el-shell-de-linux-como-ninja/bash-aliases.png)

Aquí defino alias de comando para cosas como:

* Coloreado de la salida de ls(1)
* Uso seguro de rm, mv y cp
* Coloreado de grep y amigos
* listado de logs
* Atajos de git
* otros

## Atajos de teclado

Cuando trabajamos todo el día en un shell, es recomendable no repetirse tanto. Aquí dejo una lista de atajos de
teclado que espero les sirvan para ahorrar tiempo.

* **Ctrl+**      Limpia la pantalla, hace lo mismo que el comando **clear** pero no limpia la linea actual.
* **Ctrl+r**     Busca un comando tecleado con anterioridad que empiece con las letras que tecleemos.
* **Ctrl+u**     Limpia desde la posición actual del cursor hasta el principio de la linea.
* **Ctrl+a**     Posiciona el cursos al principio de la linea de comandos.
* **Ctrl+e**     Posiciona el cursos al final de la linea de comandos.
* **Ctrl+->**    Mueve el cursor una palabra hacía adelante
* **Ctrl+<-**    Mueve el cursor una palabra hacía atras.
* **Esc+u**      Cambia a Mayúsculas la palabra adelante del cursor.
* **Esc+t**      Invierte el orden de dos palabras contiguas.
* **Esc+p**      Busca en el historial de comandos, similar a Ctrl+r
* **Ctrl+c**     Cancela la ejecución de el proceso actual.
* **Ctrl+d**     Hace logout y/o cierra la sesión actual (equivalente al comando **exit**)
* **Ctrl+h**     Borra una letra atrás del curso (igual que la tecla backspace/retroceso).
* **Ctrl+z**     Manda el proceso actual al background (segundo plano), el proceso puede ser regresado a primer
plano con los comandos: **jobs** y **fb**.

## Terminator como emulador de terminal

Terminator es un programa emulador de terminal que puede usarse desde un ambiente gráficos en sistemas GNU/Linux,
en lo personal lo uso sobre KDE.

Terminator me permite dividir la ventana en mosaico, tener sesiones shell independientes en cada recuadro, también
me permite tener multiples pestañas, y poder usar la agrupación de ventanas, lo cual me permite escribir un mismo
comando en multiples servidores vía SSH o como le llaman hacer broadcast.

![Terminator tiles](/posts/configurando-el-shell-de-linux-como-ninja/terminator-tiles.png)

En próximos artículos dedicaré más tiempo a terminator y la combinación que hago con GNU screen.

Por ahora es todo, se volvió muy largo el articulo por la cantidad de código de shell, mi última recomendación es
que sus configuraciones de shell las tengan en un sistema de control de versiones como git, en lo personal tengo
un repositorio personal y público en GitLab donde actualizo mis configuraciones de vez en cuando, el cual clono
cada vez que instalo un sistema nuevo. Si lo quieren ver este es el URL [https://gitlab.com/jorge.medina/dotfiles](https://gitlab.com/jorge.medina/dotfiles).

Si este articulo es de tu agrado y te trajo algún aprendizaje comparte lo con tus amigos. ¡Hasta la próxima!.
