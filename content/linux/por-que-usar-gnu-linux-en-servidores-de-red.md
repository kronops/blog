---
title: Por qué usar GNU Linux en servidores de red
date: 2020-01-27
categories:
  - linux
  - redes
  - servidores
---

## Introducción

En este articulo explicare porque recomendamos usar GNU/Linux como sistema operativo para servidores de red, ya sea
para uso personal o empresarial. Empezaremos hablando de analizar los requisitos para implementar un servidor de red,
porque usar GNU/Linux, y al final porque usar la distribución Ubuntu Server para servidores de red.

## Objetivos

Mostrar las ventajas tanto a nivel negocio como a nivel técnico de utilizar sistemas GNU/Linux en servidores de red
en entornos de uso personal y empresariales.

## Audiencia

Este documento esta enfocado a ayudar a administradores de sistemas, desarrolladores, o gerentes de areas de TI en
la toma de decisiones para elegir inteligente mente que sistema operativo utilizar en la infraestructura de TI de
las empresas.

## Analizando los requisitos para la implementación de un servidor de red

Antes de ponerse a instalar un sistema operativo en un servidor es recomendable hacer una pausa y pensar en los
requisitos del cliente, donde el cliente podemos ser nosotros mismos, nuestros jefes en la oficina o algún cliente
de otra empresa. Es decir, debemos realizar un análisis de requisitos.

Es importante dejar bien en claro y por escrito (el inicio de la documentación) cuales son los requisitos, tanto los
funcionales y los no funcionales, se debe establecer cuales son los problemas que se quieren resolver, de ser
necesario cuales son los requisitos del negocio y también los requisitos técnicos.

Algunos de los requisitos de negocio más comunes pueden ser:

* Reducir los costos de operación en relación a la infraestructura de TI.
* Inter conectar los equipos de computo usando una infraestructura de TI abierta y estándar la cual permita hacer
  un uso más eficiente de los recursos.
* Centralizar el almacenamiento de los documentos de oficina y datos de aplicaciones de la empresa.
* Controlar el acceso de usuarios y grupos a los recursos compartidos en la red de forma eficiente y segura.
* Mejorar la colaboración entre usuarios usando el servicio de compartición de archivos e impresoras en red.
* Mejorar las comunicaciones entre los colaboradores usando el servicio de correo electrónico.

Lo anterior se puede traducir en los siguientes requisitos técnicos:

* Crear una red LAN para conectar los equipos de computo y otros dispositivos de red, de forma que los usuarios
  puedan mejorar la productividad colaborando de forma eficiente con el uso de las tecnologías de la información y
  comunicaciones.
* Implementar los servicios de infraestructura de red básicos para el control lógico de la red.
* Implementar un sistema controlador de dominio para centralizar la administración de usuarios y grupos, así como
  el acceso a los recursos de la red.
* Implementar un servidor para ofrecer servicios de compartición de archivos e impresoras en red.
* Implementar un servidor de red para ofrecer servicios de correo electrónico.

Después de tener bien claros los requisitos, solo entonces podemos empezar a pensar en el diseño de la red, y
planificar la instalación de servidores y servicios de red.

## Por qué usar el sistema operativo GNU/Linux para un servidor de red

Los sistemas operativos GNU/Linux están basados en los principios del software libre, dicho software respeta la
libertad de los usuarios y la comunidad, ofreciendo las siguientes libertades:

* La libertad de **ejecutar** el programa como se desea, con cualquier propósito (libertad 0).
* La libertad de **estudiar** cómo funciona el programa, y cambiarlo para que haga lo que usted quiera (libertad 1).
  El acceso al código fuente es una condición necesaria para ello.
* La libertad de **redistribuir** copias para ayudar a su prójimo (libertad 2).
* La libertad de distribuir copias de sus versiones **modificadas** a terceros (libertad 3). Esto le permite ofrecer
  a toda la comunidad la oportunidad de beneficiarse de las modificaciones. El acceso al código fuente es una
  condición necesaria para ello.

En grandes líneas, significa que los usuarios tienen la libertad para ejecutar, copiar, distribuir, estudiar,
modificar y mejorar el software. Es decir, el «software libre» es una cuestión de libertad, no de precio. Para
entender el concepto, piense en «libre» como en «libre expresión», no como en «barra libre».

Además de esto, el kernel Linux es el proyecto de software que ha unido a más gente en la historia de la humanidad
como ningún otro proyecto. Esto significa que alrededor del mundo hay miles de programadores, usuarios, educadores,
comerciantes y toda clase de personas dedicadas a fomentar el uso de las tecnologías abiertas y los principios del
software libre, todo para el beneficio de la humanidad.

Al tener a tanta gente trabajando en el mismo objetivo obtenemos un sistema operativo que ofrece las siguientes
características:

* **Escalabilidad:** Los sistemas GNU/Linux permiten escalar para ofrecer servicios para usuarios en casa, empresas
  de todos tamaños, y hasta para trabajos científicos.
* **Disponibilidad:** Los sistemas GNU/Linux ofrecen tasas de disponibilidad para soportar todo tipo de demanda.
* **Rendimiento:** Los sistemas GNU/Linux ofrecen un buen rendimiento de forma predeterminada, pero además permiten
  optimizarlos para todo tipo de demandas.
* **Seguridad:** Los sistemas GNU/Linux son seguros de forma predeterminada, y cuando se presenta un problema de
  seguridad, la comunidad los arregla rápidamente teniendo las correcciones en poco tiempo.
* **Administrabilidad:** Los sistemas GNU/Linux permite ser administrados usando diferentes métodos, tanto en línea
  de comandos como con interfaces gráficas, o incluso usando herramientas dedicadas a configuraciones centralizadas y
  automatizadas.
* **Facilidad de Uso:** A pesar de la mala fama, los sistemas GNU/Linux son fáciles de usar, siempre y cuando se
  tenga la formación adecuada.
* **Adaptabilidad:** Los sistemas GNU/Linux pueden adaptarse a los cambios continuos que se presentan cada día.
* **Asequible:** Los sistemas GNU/Linux, al ser software libre no tienen costos de licencias.
* **Balance:** Los sistemas GNU/Linux ofrecen el mejor balance costo beneficio.

## Por qué elegir la distribución Ubuntu Server para un servidor de red

Algunas de las razones por las que Ubuntu Server Edition es recomendado para servidores de red:

* Ubuntu Server Edition es una distribución GNU/Linux orientada a servidores, esta basado en Debian y soporta los
  mismos paquetes y configuraciones.
* Ubuntu Server Edition esta respaldado y soportado por la empresa Canonical.
* El equipo de Ubuntu Server mantiene ciclos de control de calidad adicionales a los de Debian.
* Ubuntu Server LTS tiene un ciclo de vida de 5 años con soporte de actualizaciones de seguridad.
* Ubuntu server esta certificado para servidores DELL PowerEdge y HP Proliant entre otras marcas.
* El equipo de Ubuntu Security se encarga de desarrollar e integrar herramientas de seguridad a nivel kernel y
  aplicación, darle seguimiento a las vulnerabilidades de seguridad que afectan los paquetes soportados realizando
  auditorias, rastreos, corrección y pruebas.
* Ubuntu Server incluye soporte completo de virtualización basada en KVM el cual soporta la mayoría de funciones
  incluidas en Xen.
* Ubuntu Server LTS tiene herramientas para integrar plataformas Cloud privadas basadas en KVM.

Para conocer más acerca de Ubuntu server les recomiendo leer los brochures para las últimas versiones LTS.

## Recursos adicionales

Recomendamos leer la documentación en línea para conocer más acerca:

* [Requisitos no funcionales](http://es.wikipedia.org/wiki/Requisito_no_funcional)
* [Requisitos funcionales](http://es.wikipedia.org/wiki/Requisito_funcional)
* [Que es el software libre?](http://www.gnu.org/philosophy/free-sw.es.html)
* [Kernel Linux](https://www.kernel.org/)
* [Why Linux is Better?](http://www.whylinuxisbetter.net)
* [Ubuntu Server](http://www.ubuntu.com/server)
