+++
date = 2020-04-28
title = "Instalación de Servidor Linux Ubuntu Server 20.04"
authors = ["Jorge Medina"]
categories = [
    "linux",
    "ubuntu",
    "redes",
    "servidores",
]
+++

## Introducción

En este articulo mostrare como realizar una instalación básica del sistema operativo Ubuntu Server 20.04 LTS. Ubuntu
Server es una distribución GNU/Linux optimizada para funcionar como servidor de red cubriendo tanto instalaciones
de servidores caseros, servidores de tipo empresarial tales como granjas o clusters de servidores hasta para
infraestructura base para una nube publica o privada de servidores Linux.

Las actividades que describiré a detalle son las siguientes:

* Definición de Requisitos.
* Tipos de instalación de Ubuntu Server.
* Obteniendo la media de instalación de Ubuntu Server.
* Arranque del servidor.
* Procedimiento de instalación atendida.
* Validación de la instalación.

## Objetivos

Describir a detalle los pasos propios del procedimiento de instalación de la distribución GNU/Linux Ubuntu Server
20.04 LTS. Servir como documento base para una serie de artículos sobre la implementación de servidores de red
basados en el sistema operativo GNU/Linux, en especifico en la distribución Ubuntu Server 20.04.

## Audiencia

Este documento esta enfocado a ayudar a administradores de Sistemas, des arrolladores y otros interesados en como
instalar un servidor de red con Ubuntu Server 20.04.

## Definición de Requisitos

El procedimiento descrito lo realice en una máquina virtual, sin embargo, debería de aplicar igual para equipos
físicos, o máquinas virtuales en otro hypervisor.

Para realizar la instalación requerimos lo siguiente:

* Servidor físico o virtual con soporte CD, DVD o USB para el arranque del sistema.
* CD o USB con la media de instalación de Ubuntu 20.04.
* Configurar el BIOS para arrancar el medio de instalación seleccionado.
* Servidor físico o virtual con por lo menos una tarjeta de red Ethernet 10/100/1000.

En este ejemplo se asume lo siguiente:

* La instalación del servidor se realizará en el lenguaje Inglés y que la configuración de localización o
  configuraciones regionales son para el País México, en especial la Ciudad de México. También se asume que el
  sistema tiene un teclado con la distribución Español Latinoamericano.
* El servidor está conectado a una red donde hay un servidor DHCP que ofrece la configuración automática de los
  parámetros de red.
* El servidor tiene conectividad a Internet directamente a través del default gateway, por lo que no requiere de
  proxy HTTP para acceder a los depósitos de software para instalar las últimas versiones de los programas.

En este ejemplo realizaremos una instalación usando un esquema de particionamiento guiado donde usa todo el disco
duro, no nos meteremos en detalles para diseñar un esquema de particiones personalizado, esto lo veremos en próximas
ediciones.

## Tipos de instalación de servidores de red Ubuntu Server

Ubuntu Server 20.04 esta soportado tanto para instalaciones en máquinas físicas y máquinas virtuales.

En máquinas físicas soporta las siguiente arquitecturas:

* x86
* x86-64

Para máquinas virtuales esta soportado en los siguientes hypervisores:

* VMware
* VirtualBox
* Xen
* KVM
* Hyper-V

También esta soportado para trabajar en modo contenedor en:

* OpenVZ
* LXC

Adicional mente, Ubuntu Server 12.04 está soportado para trabajar en plataformas de Cloud Computing:

* Amazon AWS
* OpenStack
* Microsoft Azure
* Google Cloud

En las siguientes secciones veremos como instalar el sistema Ubuntu Server 20.04 usando el método que aplica tanto
para equipos físicos y virtuales.

## Obteniendo el medio de instalación de Ubuntu Server

Para instalar la distribución GNU/Linux Ubuntu Server 20.04 siga las siguientes instrucciones:

* Vaya al sitio oficial de ubuntu en el URL: [http://www.ubuntu.com](http://www.ubuntu.com).
* Después vaya a la sección de descargas o Downloads, y seleccione la opción de Ubuntu Server, o vaya directo al
  URL de descargas de ubuntu server: [http://www.ubuntu.com/download/server](http://www.ubuntu.com/download/server).
* Del lado izquierdo elija la arquitectura que desea usar

Si el el equipo en el cual se va a instalar Ubuntu Server no tiene una unidad de lectura de CDs o DVDs entonces se
recomienda transferir la imagen a un dispositivo de almacenamiento USB.

Para transferir la imagen ISO a un dispositivo USB se recomienda usar el programa [UNetbootin](http://unetbootin.sourceforge.net/).

## Arranque del servidor

Dependiendo del tipo de sistema, si es físico o virtual podemos arrancar el sistema usando los diferentes métodos:

* Vía CD o DVD de Instalación.
* Vía USB de instalación.
* Vía Red por PXE.
* Vía CD Virtual vía HP ILO, Dell iDRAC u otras BMC.

Sea cual sea el método elegido, se debe de configurar el BIOS para arrancar usando cualquier método, ya sea
especificando el método de forma permanente o temporal.

Para más información sobre los métodos de configuración de arranque del BIOS leer la documentación especifica de
su hardware, hypervisor o consola BMC.

## El procedimiento de Instalación atendida

### Seleccionando el lenguaje

Al arrancar la máquina con la media de instalación, lo primero que nos aparece es seleccionar el lenguaje con el cual
iniciará el instalador del sistema operativo, elegimos el lenguaje **English**.

![Instalación Ubuntu Server - lenguaje](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-1.png)

Después de elegir el lenguaje seleccionamos se nos muestra el menú principal, elegimos la opción **Install Ubuntu
Server** para iniciar el proceso de instalación.

![Instalación Ubuntu Server - lenguaje](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-2.png)

La siguiente pantalla nos pide seleccionar (nuevamente) el lenguaje usado en el proceso de instalación,
seleccionamos **English**.

![Instalación Ubuntu Server - lenguaje](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-3.png)

### Seleccionando la ubicación

En la siguiente pantalla debemos seleccionar la localización en la que se encuentra el sistema, esto sirve para
establecer la zona horaria y otros parámetros relacionados a la localización e internacionalización del sistema,
seleccionamos **other**, después **North America** y después **Mexico**.

![Instalación Ubuntu Server - ubicación](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-4.png)

![Instalación Ubuntu Server - ubicación](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-5.png)

![Instalación Ubuntu Server - ubicacion](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-6.png)

### Configurando las locales

En la siguiente pantalla nos pide definir la locale para el sistema, esto es porque no hay una combinación para
el lenguaje elegido (English) y el país seleccionado (Mexico), por lo tanto seleccionamos la opción
**United States - en_US.UTF-8**.

![Instalación Ubuntu Server - locales](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-7.png)

### Configurando el teclado

En la siguiente pantalla debemos elegir la distribución del teclado que usaremos en el sistema, nos pregunta si
queremos usar la detección de la distribución, en este caso le diremos **No** para elegir una distribución
manualmente, y en la siguiente pantalla elegimos **Spanish (Latin American)**.

![Instalación Ubuntu Server - teclado](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-8.png)

![Instalación Ubuntu Server - teclado](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-9.png)

![Instalación Ubuntu Server - teclado](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-10.png)

### Configurando la red

En las siguientes pantallas mostrara la detección los diálogos de detección de hardware, en especifico para la
configuración de la red, si en la red en donde está conectado el servidor tenemos un servidor DHCP, como es en
nuestro caso, entonces configura la red automáticamente.

![Instalación Ubuntu Server - red](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-11.png)

En la siguiente pantalla nos solicita establecer el nombre del host (hostname), el cual debe de ser una sola
palabra, en este caso usare el nombre **neti01**.

![Instalación Ubuntu Server - red](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-12.png)

### Configurando usuarios y contraseñas

En la siguiente pantalla nos solicita establecer el nombre completo de la cuenta de administrador, en este caso
uso **System Administrator**:

![Instalación Ubuntu Server - usuarios](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-13.png)

Después nos pide establecer el nombre de la cuenta para System Administrator, en este caso uso el nombre de
cuenta **sysadmin**.

![Instalación Ubuntu Server - usuarios](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-14.png)

En las siguientes pantallas debemos de establecer la contraseña para la cuenta antes creada, también la debemos
repetir para confirmarla. En este caso escribo **sysadmin**.

![Instalación Ubuntu Server - usuarios](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-15.png)

En caso de querer mostrar la contraseña en texto plano para verificar que se escribió correctamente seleccione
la opción `Show Password in Clear`, luego tendrá que escribir nuevamente la contraseña para verificarla:

![Instalación Ubuntu Server - usuarios](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-16.png)

**NOTA:** Debe escribir una contraseña segura, la cual debe de tener por lo menos 8 caracteres, incluir caracteres
mayúsculas y minúsculas, números y caracteres especiales.

### Configurando el reloj

En la siguiente pantalla nos pide definir la zona horaria en la que se encuentra el equipo, el sistema trato de
detectarla, en nuestro caso usamos **America/Mexico_City**, si no es la deseada la debe cambiar.

![Instalación Ubuntu Server - reloj](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-17.png)

**IMPORTANTE:** Asegúrese de establecer una zona horaria correcta para su localidad, ya que de esto dependerá que
el reloj del sistema funcione correctamente y que también se haga el cambio de horario en automático cuando haya
cambio de horario de verano o invierno.

### Partición de discos

En la siguiente pantalla se nos muestran los discos conectados a la máquina, luego nos pide seleccionar el método
de particionamiento de disco, en este caso, y para simplificar la instalación elegimos la opción `Guided partitioning`:

![Instalación Ubuntu Server - discos](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-18.png)

Y en la siguiente ventana seleccionamos el método **Guided - use entire disk**.

![Instalación Ubuntu Server - discos](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-19.png)

Después nos pide seleccionar el disco sobre el cual aplicaremos las particiones, en este caso solo tenemos un solo
disco que es reconocido como **vda** de 28.6 GB.

![Instalación Ubuntu Server - discos](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-20.png)

En la siguiente pantalla nos muestra un resumen del disco que particionara, las particiones y sistemas de archivos
que va a crear y nos pide confirmación, elegimos **Yes**.

![Instalación Ubuntu Server - discos](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-21.png)

![Instalación Ubuntu Server - discos](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-22.png)

### Instalando el sistema base

Después de terminar el particionamiento el sistema iniciará a realizar la instalación del sistema base.

![Instalación Ubuntu Server - base](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-23.png)

![Instalación Ubuntu Server - base](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-24.png)

### Configurando el gestor de paquetes apt

Antes de terminar de instalar el sistema base, el instalador tendrá que instalar los últimos paquetes desde los
depósitos de paquetes desde Internet, por lo que pregunta si usamos un servidor proxy HTTP para acceder a Internet,
en este caso no usamos un proxy por lo que elegimos **Continue**.

![Instalación Ubuntu Server - paquetes](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-25.png)

![Instalación Ubuntu Server - paquetes](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-26.png)

### Configurando tasksel

Después de configurar APT y descargar los paquetes desde Internet, el instalador pregunta el método que usaremos
para mantener los paquetes del sistema actualizados, en este caso elegimos **No automatic updates**.

![Instalación Ubuntu Server - tasksel](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-27.png)

En la siguiente pantalla, nos pregunta si queremos instalar algún programa o grupo de programas para servidores,
en este caso, solo elegimos **OpenSSH server** y continuamos.

![Instalación Ubuntu Server tasksel](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-28.png)

![Instalación Ubuntu Server - tasksel](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-29.png)

![Instalación Ubuntu Server - tasksel](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-30.png)

### Instalando el gestor de arranque en el disco

Una vez que ha instalado todos los programas del sistema base y adicionales, nos pregunta confirmar si deseamos
instalar el gestor de arranque GRUB en el _Master Boot Record_ o _MBR_, elegimos **Yes**.

![Instalación Ubuntu Server - gestor de arranque](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-31.png)

![Instalación Ubuntu Server - gestor de arranque](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-32.png)

### Terminando la instalación

Si todo fue bien, la última pantalla nos confirma que la instalación fue termino y nos pide remover la media de
instalación y reiniciar el sistema, elegimos **Continue**.

![Instalación Ubuntu Server - terminando instalación](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-33.png)

Después de reiniciar el sistema, si el sistema arranca sin ningún problema nos lanzará la terminal con el programa
login para iniciar sesión:

![Instalación Ubuntu Server - terminando instalación](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-34.png)

Ahora realizaremos algunas tareas para validar que la instalación del sistema fue exitosa y para conocer más sobre
el sistema recién instalado.

## Validando la instalación

En esta sección mostrare los procedimientos para validar la correcta instalación del servidor, usaremos solo
programas en la línea de comandos para conocer el sistema.

Las actividades son las siguientes:

* **Inicio de sesión usuario normal** - Después de que escribe las credenciales de acceso, si estás son correctas,
  nos regresará el mensaje **MOTD** (_Message Of The Day_), el cual nos da la bienvenida, nos muestra la versión del
  sistema operativo, referencias a documentación y otra información del sistema como: carga del sistema, número
  de procesos, uso de espacio en disco, usuarios firmados, uso de memoria, dirección IP principal y uso de swap.
  Por último nos muestra el prompt.

![Instalación Ubuntu Server - login](/posts/instalacion-servidor-linux-ubuntu-server-20-04/instalacion-ubuntu-server-2004-lts-35.png)

* **Checar elevación de privilegios con sudo** - Para realizar tareas administrativas, requerimos elevar los
  privilegios del usuario no privilegiado, usemos el comando **sudo(8)** para ejecutar comandos con los
  privilegios del usuario root, por ejemplo:

```shell
sysadmin@neti01:~$ sudo whoami
[sudo] password for sysadmin:
root
```

**IMPORTANTE:** Recuerde que sudo guarda en una memoria cache la contraseña durante 5 minutos, por lo que durante
los siguientes 5 minutos no tendrá que escribir la contraseña nuevamente.

Si desea convertirse por completo como usuario root use:

```shell
sysadmin@neti01:~$ sudo -i
[sudo] password for sadmin:
root@neti01:~#
```

Para regresar como el usuario sadmin use el comando **exit**, logout o **Ctrl+D**, por ejemplo:

```shell
# exit
logout
$
```

* **Checar Sistema operativo y kernel** - Para identificar la información del sistema operativo use el comando
  **uname(1)**, por ejemplo:

```shell
sysadmin@neti01:~$ uname -a
Linux neti01 5.4.0-42-generic #46-Ubuntu SMP Fri Jul 10 00:24:02 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
```

* **Checar información del CPU** - Para identificar el CPU del sistema y sus detalles mostramos el contenido
  del archivo `/proc/cpuinfo`, por ejemplo:

```shell
sysadmin@neti01:~$ cat /proc/cpuinfo
processori : 0
vendor_id : GenuineIntel
cpu family : 6
model : 42
model name : Intel(R) Core(TM) i7-2600 CPU @ 3.40GHz
stepping : 7
microcode : 0x2f
cpu MHz : 3392.292
cache size : 16384 KB
physical id : 0
siblings : 1
core id : 0
cpu cores : 1
apicid : 0
initial apicid : 0
fpu : yes
fpu_exception : yes
cpuid level : 13
wp : yes
flags : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 syscall nx rdtscp lm constant_tsc arch_perfmon rep_good nopl xtopology cpuid tsc_known_freq pni pclmulqdq vmx ssse3 cx16 pdcm pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx hypervisor lahf_lm cpuid_fault pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid tsc_adjust xsaveopt arat umip md_clear arch_capabilities
bugs : cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs
bogomips : 6784.58
clflush size : 64
cache_alignment : 64
address sizes : 36 bits physical, 48 bits virtual
power management:

processor : 1
vendor_id : GenuineIntel
cpu family : 6
model : 42
model name : Intel(R) Core(TM) i7-2600 CPU @ 3.40GHz
stepping : 7
microcode : 0x2f
cpu MHz : 3392.292
cache size : 16384 KB
physical id : 1
siblings : 1
core id : 0
cpu cores : 1
apicid : 1
initial apicid : 1
fpu : yes
fpu_exception : yes
cpuid level : 13
wp : yes
flags : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 syscall nx rdtscp lm constant_tsc arch_perfmon rep_good nopl xtopology cpuid tsc_known_freq pni pclmulqdq vmx ssse3 cx16 pdcm pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx hypervisor lahf_lm cpuid_fault pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid tsc_adjust xsaveopt arat umip md_clear arch_capabilities
bugs : cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs
bogomips : 6784.58
clflush size : 64
cache_alignment : 64
address sizes : 36 bits physical, 48 bits virtual
power management:
```

* **Checar información de Memoria** - Para ver la información de la memoria del sistema, usamos el comando
  **free(1)**, por ejemplo:

```shell
sysadmin@neti01:~$ free
              total        used        free      shared  buff/cache   available
Mem:        4030588      124912     3646312         812      259364     3673120
Swap:       1190340           0     1190340

sadmin@neti01:~$ free -m
              total        used        free      shared  buff/cache   available
Mem:           3936         122        3560           0         253        3586
Swap:          1162           0        1162
```

* **Checar información de discos duros, particiones y sistemas de archivos** - Para mostrar información de los
  discos duros del sistema vemos el contenido del archivo `/proc/scsi/scsi`, por ejemplo:

```shell
sysadmin@neti01:~$ cat /proc/scsi/scsi
Attached devices:
Host: scsi0 Channel: 00 Id: 00 Lun: 00
  Vendor: QEMU     Model: QEMU DVD-ROM     Rev: 2.5+
  Type:   CD-ROM                           ANSI  SCSI revision: 05
```

 Para las particiones usamos el comando **parted(8)**, por ejemplo:

```shell
sysadmin@neti01:~$ sudo parted /dev/vda print
Model: Virtio Block Device (virtblk)
Disk /dev/vda: 26.8GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:

Number  Start   End     Size    Type      File system  Flags
 1      1049kB  538MB   537MB   primary   fat32        boot
 2      539MB   26.8GB  26.3GB  extended
 5      539MB   26.8GB  26.3GB  logical   ext4

```

 Para mostrar los sistemas de archivos montados y su espacio usamos el comando **mount(8)** y **df(1)**, por ejemplo:

```shell
sysadmin@neti01:~$ mount
sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,relatime)
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
udev on /dev type devtmpfs (rw,nosuid,noexec,relatime,size=1971440k,nr_inodes=492860,mode=755)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000)
tmpfs on /run type tmpfs (rw,nosuid,nodev,noexec,relatime,size=403060k,mode=755)
/dev/vda5 on / type ext4 (rw,relatime,errors=remount-ro)
securityfs on /sys/kernel/security type securityfs (rw,nosuid,nodev,noexec,relatime)
tmpfs on /dev/shm type tmpfs (rw,nosuid,nodev)
tmpfs on /run/lock type tmpfs (rw,nosuid,nodev,noexec,relatime,size=5120k)
tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,mode=755)
cgroup2 on /sys/fs/cgroup/unified type cgroup2 (rw,nosuid,nodev,noexec,relatime,nsdelegate)
cgroup on /sys/fs/cgroup/systemd type cgroup (rw,nosuid,nodev,noexec,relatime,xattr,name=systemd)
pstore on /sys/fs/pstore type pstore (rw,nosuid,nodev,noexec,relatime)
none on /sys/fs/bpf type bpf (rw,nosuid,nodev,noexec,relatime,mode=700)
cgroup on /sys/fs/cgroup/perf_event type cgroup (rw,nosuid,nodev,noexec,relatime,perf_event)
cgroup on /sys/fs/cgroup/blkio type cgroup (rw,nosuid,nodev,noexec,relatime,blkio)
cgroup on /sys/fs/cgroup/net_cls,net_prio type cgroup (rw,nosuid,nodev,noexec,relatime,net_cls,net_prio)
cgroup on /sys/fs/cgroup/freezer type cgroup (rw,nosuid,nodev,noexec,relatime,freezer)
cgroup on /sys/fs/cgroup/cpuset type cgroup (rw,nosuid,nodev,noexec,relatime,cpuset)
cgroup on /sys/fs/cgroup/memory type cgroup (rw,nosuid,nodev,noexec,relatime,memory)
cgroup on /sys/fs/cgroup/hugetlb type cgroup (rw,nosuid,nodev,noexec,relatime,hugetlb)
cgroup on /sys/fs/cgroup/cpu,cpuacct type cgroup (rw,nosuid,nodev,noexec,relatime,cpu,cpuacct)
cgroup on /sys/fs/cgroup/pids type cgroup (rw,nosuid,nodev,noexec,relatime,pids)
cgroup on /sys/fs/cgroup/rdma type cgroup (rw,nosuid,nodev,noexec,relatime,rdma)
cgroup on /sys/fs/cgroup/devices type cgroup (rw,nosuid,nodev,noexec,relatime,devices)
systemd-1 on /proc/sys/fs/binfmt_misc type autofs (rw,relatime,fd=28,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=15692)
hugetlbfs on /dev/hugepages type hugetlbfs (rw,relatime,pagesize=2M)
mqueue on /dev/mqueue type mqueue (rw,nosuid,nodev,noexec,relatime)
debugfs on /sys/kernel/debug type debugfs (rw,nosuid,nodev,noexec,relatime)
tracefs on /sys/kernel/tracing type tracefs (rw,nosuid,nodev,noexec,relatime)
fusectl on /sys/fs/fuse/connections type fusectl (rw,nosuid,nodev,noexec,relatime)
configfs on /sys/kernel/config type configfs (rw,nosuid,nodev,noexec,relatime)
/dev/vda1 on /boot/efi type vfat (rw,relatime,fmask=0077,dmask=0077,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro)
tmpfs on /run/user/1000 type tmpfs (rw,nosuid,nodev,relatime,size=403056k,mode=700,uid=1000,gid=1000)

sadmin@neti01:~$ df -h
Filesystem      Size  Used Avail Use% Mounted on
udev            1.9G     0  1.9G   0% /dev
tmpfs           394M  812K  393M   1% /run
/dev/vda5        24G  3.4G   20G  15% /
tmpfs           2.0G     0  2.0G   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
tmpfs           2.0G     0  2.0G   0% /sys/fs/cgroup
/dev/vda1       511M  4.0K  511M   1% /boot/efi
tmpfs           394M     0  394M   0% /run/user/1000
```

* **Checar información de la red local** - Para mostrar información acerca de la tarjeta de red del sistema usar
  el comando **lspci(8)**, por ejemplo:

```shell
sysadmin@neti01:~$ lspci | grep Ethernet
01:00.0 Ethernet controller: Red Hat, Inc. Virtio network device (rev 01)
```

Para ver información acerca de la interfaz de red use el comando **ip(8)**, por ejemplo:

```shell
sysadmin@neti01:~$ ip address list
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:3b:00:d7 brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.248/24 brd 192.168.122.255 scope global dynamic enp1s0
       valid_lft 2418sec preferred_lft 2418sec
    inet6 fe80::5054:ff:fe3b:d7/64 scope link
       valid_lft forever preferred_lft forever
```

Para ver la información de la tabla de rutas e identificar el default gateway use el comando **ip(8)**, por ejemplo:

```shell
sysadmin@neti01:~$ ip route list
default via 192.168.122.1 dev enp1s0 proto dhcp src 192.168.122.248 metric 100
192.168.122.0/24 dev enp1s0 proto kernel scope link src 192.168.122.248
192.168.122.1 dev enp1s0 proto dhcp scope link src 192.168.122.248 metric 100
```

La dirección IP del default gateway es la segunda columna de la línea que empieza con default, en este caso
**192.168.122.1**.

Use el comando **ping(8)** para validar la conectividad a nivel LAN haciendo ping al default gateway, por ejemplo:

```shell
sysadmin@neti01:~$ ping 192.168.122.1 -c3
PING 192.168.122.1 (192.168.122.1) 56(84) bytes of data.
64 bytes from 192.168.122.1: icmp_seq=1 ttl=64 time=0.143 ms
64 bytes from 192.168.122.1: icmp_seq=2 ttl=64 time=0.280 ms
64 bytes from 192.168.122.1: icmp_seq=3 ttl=64 time=0.215 ms

--- 192.168.122.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2047ms
rtt min/avg/max/mdev = 0.143/0.212/0.280/0.055 ms
```

Por ahora esta información es suficiente para validar que el sistema está funcionando correctamente, en los
siguientes artículos publicaremos tareas de administración del sistema.

## Recursos adicionales

Para más información acerca de los comandos utilizados se recomienda leer las páginas de los manuales de cada uno
de ellos.

* login (1) - begin session on the system
* sudo (8) - execute a command as another user
* uname (1) - print system information
* free (1) - Display amount of free and used memory in the system
* parted (8) - a partition manipulation program
* mount (8) - mount a filesystem
* df (1) - report file system disk space usage
* lspci (8) - list all PCI devices
* ip (8) - show / manipulate routing, network devices, interfaces and tunnels
* ping (8) - send ICMP ECHO_REQUEST to network hosts

Recomendamos leer la documentación en línea para conocer más acerca de Ubuntu Server:

* Ubuntu Server: [http://www.ubuntu.com/server](http://www.ubuntu.com/server)
* Ubuntu Server Guide: [https://ubuntu.com/server/docs](https://ubuntu.com/server/docs)
* Ubuntu Server FAQ: [https://help.ubuntu.com/community/ServerFaq](https://help.ubuntu.com/community/ServerFaq)
