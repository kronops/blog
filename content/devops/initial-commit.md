+++
date = 2020-01-28
title = "Initial Commit"
subtitle = "Por qué necesitamos la automatización de procesos de TI"
authors = ["Jorge Medina"]
categories = [
    "automatizacion",
    "infraestructura",
    "operaciones",
]
+++

En los últimos años he convivido, trabajado y colaborado con personas que trabajan en áreas de TI, principalmente en
los departamentos de infraestructura y operaciones. Ellos tienen algo en común: todos buscan una mejor forma de
trabajar con la infraestructura y hacer más simples los procesos de operaciones de TI.

Los procesos de la infraestructura de TI, como la administración de redes de datos y servidores, y los de operación
de TI, como la gestión de incidentes y el monitoreo, se basan en modelos del pasado, cuando los requisitos de los
clientes eran fijos y con poco margen de cambio. Hoy en día es necesario dar respuesta a la demanda de este tipo de
recursos y servicios de TI de una manera dinámica y en el menor tiempo, es decir, es necesario ser ágil.

Esto nos ha llevado a buscar nuevas formas de hacer las cosas. Buscamos las mejores prácticas de la industria, las
mejores metodologías y marcos de trabajo, pero ahora con un enfoque centrado en las personas, luego en los productos
y la tecnología.

La automatización de procesos es una práctica que permite simplificar la manera de llevarlos a cabo, ya que el día a
día de la administración de sistemas suele estar lleno de rutinas repetitivas, que no aportan mucho valor y al
realizarlas de forma manual se introduce el riesgo de error humano. Este tipo de rutinas incluyen la instalación de
servidores físicos, hipervisores, máquinas virtuales, el sistema operativo del host y de las VMs, así como la
configuración de servicios básicos del sistema, el despliegue de servidores de aplicaciones, servidores web,
servidores de bases de datos, también la auditoria y remediación de hallazgos de seguridad, y la más importante
del día a día es el despliegue de actualizaciones de las aplicaciones.

Lo que necesitamos es automatizar todas estas rutinas, convertir esos procesos que ahora solo son conocimiento
empírico en procedimientos repetibles y reproducibles, lo cual nos llevara a mejorar la consistencia en la entrega
de los servicios que ofrecemos en las áreas de infraestructura y operaciones de TI.

En este blog hablaremos de como un grupo de de administradores de sistemas, arquitectos y desarrolladores hemos
descubierto nuevas formas de hacernos la vida más fácil automatizando estos procesos, que prácticas hemos adoptado,
como las implementamos con las herramientas disponibles en el mundo open source y como las aplicamos a la tecnología
en los centros de datos y nubes que administramos.

Hablaremos de conceptos, principios y prácticas ágiles que nos llevarán a un mundo con menos fricción y más
colaboración y comunicación entre los equipos.

Si te interesan estos temas no te pierdas las próximas publicaciones, donde empezaremos con los principios de la
automatización de TI  y poco a poco introduciendonos en nuevos temas del mundo de DevOps.
