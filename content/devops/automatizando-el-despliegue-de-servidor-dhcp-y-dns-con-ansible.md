+++
date = 2021-07-03
title = "Automatizando el despliegue de servidor DHCP y DNS con Ansible"
description = "Configuraciones de servidor docker y pi-hole"
authors = ["Jorge Medina"]
categories = [
    "ansible",
    "automatizacion",
    "configuration management",
    "docker",
    "linux",
]
+++

Saludos a todos!, nuevamente quiero agradecerles por tomarse el tiempo de aprender conmigo con estos artículos. Esta
ya es la tercer entrega, y como lo dijimos, en esta ocasión vamos a instalar un servidor DHCP y DNS en la Raspberry
que ya preparamos en la [segunda](https://www.kronops.com.mx/blog/random-1/post/automatizando-el-despliegue-de-servidores-linux-con-ansible-10) entrega de esta serie de Ansible. Usaremos docker para desplegar [pi-hole](https://pi-hole.net/)
como contenedor, por lo tanto tendremos que configurar algunas cosas extras, claro todo en forma automática.

## Introducción

En este tercer articulo nos enfocaremos en implementar un servidor [DHCP](https://es.wikipedia.org/wiki/Protocolo_de_configuraci%C3%B3n_din%C3%A1mica_de_host)
y [DNS](https://es.wikipedia.org/wiki/Sistema_de_nombres_de_dominio), estos servicios serán la base de la red privada,
usaremos el sistema pi-hole, el cual integra el servicio DNS que es usado para resolver nombres de dominio a
direcciones IP. Aunque este servicio normalmente lo provee el router de Internet que te entrega tu ISP, yo prefiero
quitarle esa carga de trabajo al router, y en su lugar poner un servidor Linux con el servicio donde yo lo pueda
controlar, así me divierto teniendo servicios de red en Linux, y le quito carga al router de Internet. Con pi-hole
también podremos agregar listas negras DNS para poder bloquear el acceso a sitios de publicidad, spam y otros
contenidos no productivos y hasta maliciosos.

Pi-hole también incluye soporte para el servicio DHCP, por medio de este servicio asignaremos los parámetros de
configuración de red automáticamente a los dispositivos de la red casera, este servicio también es esencial, en lo
personal prefiero poner este servicio en un servidor sobre el cual tenga completo control, a que lo haga el DHCP del
router que me entrega mi ISP, prefiero quitarle carga al router para que se enfoque el recurso en el enrutamiento y
no en asignar direcciones IP a los dispositivos de la red.

Recuerden que el código de ejemplo que crearemos durante este articulo lo pueden descargar de Gitlab en el siguiente
URL: [https://gitlab.com/jorge.medina/ansible-pihole](https://gitlab.com/jorge.medina/ansible-pihole).

## Preparativos

Recordemos que en el [primer](https://www.kronops.com.mx/blog/random-1/post/instalacion-y-configuracion-de-ansible-en-linux-9)
articulo instalamos ansible en **node01**, configuramos la llave SSH en **rpi**, el cual ya está en el inventario con
los parámetros de conexión y finalmente realizamos una prueba de conectividad. En el [segundo](https://www.kronops.com.mx/blog/random-1/post/automatizando-el-despliegue-de-servidores-linux-con-ansible-10)
articulo realizamos configuraciones generales el sistema, com establecer el hostname, las conexiones de red WIFI,y
Ethernet y la configuración del servicio SSH para la administración remota con algunos parámetros de seguridad
básicos. Trabajaremos en el directorio **/etc/ansible** de la máquina **node01**.

## Despliegue de Docker

En esta sección describo como crear el rol para el servicio docker, en el cual realizaremos actividades como:

1. Configurar soporte cgroups para memoria y cpu en el kernel
2. Instalar docker y dependencias python
3. Iniciar el servicio docker
4. Dar permisos a docker

Para gestionar los roles nos cambiamos al directorio **/etc/ansible/roles**:

```shell
cd /etc/ansible/roles
```

Usamos el comando **ansible-galaxy** con la opción **init** para crear la estructura de archivos y directorios del rol:

```shell
ansible-galaxy init docker
```

Veamos las tareas que definimos en el rol de **docker**, esto se define en el archivo **tasks/main.ym**l:

```shell
vim docker/tasks/main.yml
```

![Ansible role docker tasks](/posts/automatizando-el-despliegue-de-servidor-dhcp-y-dns-con-ansible/ansible-role-docker-tasks.png)

En esta lista de tareas realizamos lo siguiente:

* Agregar el soporte cgroups al kernel modificando una línea en /boot/cmdline.txt usando el módulo **lineinfile**.
* Reiniciar la máquina con el comando shutdown usando el módulo **shell**.
* Esperar a que el nodo reinicie usando el módulo **wait_for_connection**.
* Instalar dependencia de docker con el módulo **apt**.
* Crear enlace simbólico del binario de pip usando el módulo **file**.
* Instalar la llave del repositorio apt de docker para raspberry con el módulo **apt_key**.
* Instalar el repositorio de apt para docker usando el módulo **apt_repository**.
* Instalar la última versión del paquete docker-ce con el módulo **apt**.
* Dar privilegios de uso de docker agregando el usuario pi al grupo docker con el módulo **user**.
* Iniciar el servicio docker y habilitarlo al arranque del sistema con el módulo **service**.

La tarea de instalación del repositorio apt de docker usa una variable ansible, en especifico
**{{ docker_release }}**, esta debe esta definida en el archivo **defaults/main.yml**:

```shell
vim docker/defaults/main.yml
```

![Ansible role docker defaults](/posts/automatizando-el-despliegue-de-servidor-dhcp-y-dns-con-ansible/ansible-role-docker-defaults.png)

## Despliegue de Pi-hole

Ahora mostraremos como crear un rol de ansible para configurar el servicio del servidor Pi-hole.  Usamos el comando
**ansible-galaxy** con la opción **init** para crear la estructura de archivos y directorios del rol:

```shell
ansible-galaxy init pi-hole
```

Veamos las tareas que definimos en el rol de **pi-hole**, esto se define en el archivo **tasks/main.ym**l:

```shell
vim pi-hole/tasks/main.yml
```

![Ansible role pi-hole tasks](/posts/automatizando-el-despliegue-de-servidor-dhcp-y-dns-con-ansible/ansible-role-pi-hole-tasks.png)

En la primera parte del playbook se realizan las siguientes tareas:

* Instalar con el módulo **apt** las últimas versiones de los paquetes de dependencias requeridas**.**
* Usar el módulo file para crear el directorio home de pihole /home/pi/pihole con los permisos y privilegios.
* Crear segundo directorio en el home de pihole con el módulo **file**.
* Crear archivo de configuración para [FTL](https://github.com/pi-hole/FTL) con módulo **blockinfile**.
* Iniciar contenedor pihole con módulo **docker_container**, en esta tarea se definen varios parámetros:
  * **name:** Se define el nombre del contenedor a partir de la variable **{{ pihole_name }}**.
  * **image:** Se define el nombre y versión de la imagen docker a desplegar a partir de variable **{{ pihole_image }}**.
  * **restart_policy:** Se define la política de reinicio del contenedor.
  * **env:** Se definen las variables de ambiente usadas para la configuración de pi-hole, tales como, la zona
    horaria, la contraseña del usuario administrador de la consola web, las direcciones de los servidores DNS que
    usaremos como upstream o forwarders, configuraciones de dnsmasq, y los parámetros para activar el servicio DHCP
    y configurar lo.
  * **dns_servers:** Se definen los servidores dns que usan los contenedores.
  * **network_mode:** Se define el modo de red para los contenedores docker desde la variable **{{ pihole_network_mode }}**.
  * **capabilities:** Se definen los capabilites de **NET_ADMIN** en el kernel requeridos para soportar DHCP.
  * **volumes:** Se definen los volúmenes que usarán los contenedores para almacenamiento persistente, uno para la
    configuración de pihole y otro para la de dnsmasq.
  * **log_driver:** Se define el formato de los logs a JSON.
  * **log_options:** Se definen opciones de tamaños en los logs.

La segunda parte del playbook es la siguiente:

![Ansible roke pi-hole tasks extra](/posts/automatizando-el-despliegue-de-servidor-dhcp-y-dns-con-ansible/ansible-role-pi-hole-tasks-extra.png)

En la segunda parte del playbook se realizan las siguientes tareas:

* Crear directorio /opt/pihole con el módulo **file**.
* Instalar el archivo /opt/pihole/docker-compose.yml con el módulo **template**.
* Revisar el estado el servicio del HTTP de pihole con el módulo **uri**.
* Eliminar versiones viejas de la imagen docker de pihole con el módulo **docker_prune**.

Las tareas que están parametrizadas para usar una variable de ansible usarán los valores predeterminados definidos
en el archivo **defaults/main.yml**:

```shell
vim general-settings/defaults/main.yml
```

El contenido es este:

```yaml
---
# defaults file for pi-hole

# system
timezone: "America/Mexico_City"
# pi-hole
pihole_ftl_max_db_days: "180"
pihole_name: "pihole"
pihole_serverip: "0.0.0.0"
pihole_image: "pihole/pihole:v5.8.1-armhf-buster"
pihole_webpassword: "p1holeS3cr3t##"
pihole_dns1: "94.140.14.14"
pihole_dns2: "94.140.15.15"
pihole_network_mode: host
dhcp_active: "true"
dhcp_start: "192.168.3.201"
dhcp_end: "192.168.3.251"
dhcp_router: "192.168.3.1"
dhcp_leasetime: "24"
pihole_domain: "home.lan"

# adlists
# http://sysctl.org/cameleon/hosts
# https://dbl.oisd.nl/
# https://hosts-file.net/ad_servers.txt
```

Note que es en esta lista de variables donde se definen los parámetros predefinidos para este rol, sin embargo,
se pueden cambiar desde el inventario si se busca algo más personalizado. Aquí usamos los servidores DNS
de [adwords](https://adguard.com/es/adguard-dns/overview.html).

Agregamos el rol al playbook de despliegue:

```shell
cd /etc/ansible
vim deploy.yml
```

Las líneas que agregaremos dentro del bloque de roles son:

* docker
* pi-hole

## Pruebas y ejecución de playbooks de Ansible

Ahora que hemos terminado de editar los documentos de el rol de docker y pi-hole, debemos hacer unas pruebas de
sintaxis a los documentos para estar seguros que lo que escribimos no tiene defectos. Usaremos el comando
**ansible-playbook** y la opción **--syntax-check** y el nombre del archivo del playbook que invoca el rol.

Como se puede ver en los comandos anteriores, en esta ocasión no tenemos errores en la sintaxis, por lo tanto ya
podemos ejecutar el playbook y ver que se ejecuten las tareas en la máquina rpi:

![Ansible role pi-hole deploy 1](/posts/automatizando-el-despliegue-de-servidor-dhcp-y-dns-con-ansible/ansible-role-pi-hole-deploy-1.png)
![Ansible role pi-hole deploy 2](/posts/automatizando-el-despliegue-de-servidor-dhcp-y-dns-con-ansible/ansible-role-pi-hole-deploy-2.png)
![Ansible role pi-hole deploy 3](/posts/automatizando-el-despliegue-de-servidor-dhcp-y-dns-con-ansible/ansible-role-pi-hole-deploy-3.png)

Esto salio perfecto!, como podemos ver en la imagen, al final vemos el resumen en donde **29** tareas se ejecutaron
con resultado **OK**, algunas se saltaron, y ninguna cambio o fallo.

## Puesta a en producción

Para poder usar el servicio DHCP y DNS de la raspberry es importante asegurarse que se desactive el servicio DHCP
y DNS del router de Internet que te provee el ISP, no pueden estar en ejecución los servicios en ambos dispositivos,
si se hace tendrán conflictos.

Para administrar el servicio estando en operación se recomienda iniciar sesión en un shell del contenedor y usar el
comando pihole, para más información de su uso ver la documentación de referencia.

Con este articulo terminamos la serie de Ansible y pi-hole, en otros artículos quizás vuelva a hablar de pi-hole ya
que es algo que uso en mi casa y lo recomiendo ampliamente, espero también hablar de como usar Ansible para otras
tareas. Hasta pronto!.

## Referencias

La siguiente es una lista de documentación de referencia que pueden consultar para aprender más de pi-hole:

* [Requisitos para Pi-hole](https://docs.pi-hole.net/main/prerequisites/)
* [Docker Pi-hole](https://hub.docker.com/r/pihole/pihole)
* [Pi-hole Docker DHCP and network modes](https://docs.pi-hole.net/docker/DHCP/)
* [Pihole command](https://docs.pi-hole.net/core/pihole-command/)
