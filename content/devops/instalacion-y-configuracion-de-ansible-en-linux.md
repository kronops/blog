+++
date = 2021-06-13
title = "Instalación y Configuración de Ansible en Linux"
description = "Preparando el sistema de automatización de configuraciones del sistema"
authors = ["Jorge Medina"]
categories = [
    "ansible",
    "automatizacion",
    "configuration management",
    "linux",
    "shell scripts",
]
+++

Es un gusto saludar a todos los que gustan de leer este blog, en esta ocasión les vengo a hablar de como podemos
usar Ansible, esta herramienta que nos permite administrar las configuraciones de nuestros servidores Linux de
forma automatizada. Mostraremos como crear el código de ansible, mejor conocido como playbooks, para poder
automatizar diferentes tareas que son comunes después de instalar el sistema operativo, en este caso pondremos
a Debian 10 (_buster_) como sistema operativo base, usaremos una [Raspberry Pi](https://www.raspberrypi.org/)
3 B+ con [Raspberry Pi OS](https://www.raspberrypi.org/software/) (antes Rasbpian) para hacer la configuración del
servidor de red casero.

Esta es la primer de tres publicaciones sobre Ansible para automatizar las configuraciones en una Raspberry Pi.

## Introducción

Es normal que después de instalar el sistema operativo en un servidor, debamos realizar una serie de actividades de
configuración que son de rutina, estas tareas post instalación son repetitivas y por lo tanto debemos automatizarlas
para que podamos crear un procedimiento repetible y ganar consistencia en las configuraciones de los sistemas que
están a nuestro cargo y a los que les damos soporte en la red de casa. En este caso vamos a preparar un servidor
pequeñito basado en la arquitectura ARM que es muy eficiente, al principio realizaremos las configuraciones iniciales,
luego configuraremos los parámetros generales del sistema, el servicio ntp y el servicio SSH que son esenciales para
todo servidor, y en una siguiente publicación mostraremos como pondremos un servidor DHCP y DNS para la red casera
basada en [pi-hole](https://pi-hole.net/).

### Objetivo

Empezaremos desde lo básico, es decir, desde la definición de los requisitos de instalación, hasta la ejecución,
estás son las actividades especificas:

* Definición de requisitos
* Instalación de Ansible
* Configuración de Ansible
* Pruebas de conexión ansible

## Requisitos

Para poder seguir está guía es importante tener por lo menos dos servidores disponibles con Debian 10, una máquina
será la máquina de control y la otra la máquina controlada. En este ejercicio usaremos estos sistemas:

* Servidor control: **node01**
* Servidor controlado: **rpi**

Los dos servidores deben tener correctamente configurada la red, es decir, debe tener una dirección IP asignada,
de preferencia una IP fija, se deben permitir las conexiones al puerto TCP/22 para las conexiones SSH, esto es un
requisito obligatorio para usar ansible.

En cada servidor necesitamos acceso vía SSH con una cuenta de usuario que tenga permisos para escalar privilegios
usando sudo. Esta cuenta de usuario ya debe de tener configurada la llave SSH autorizada.

Los dos servidores deben tener conexión a Internet para poder descargar algunos paquetes desde diferentes repositorios
públicos.

## Instalación de Ansible

Para instalar ansible en un servidor con debian suelo usar un script que le llamo **bootstrap.sh**, veamos que hace:

```shell
mkdir bin
vim bin/bootstsrap.sh
```

El contenido del script es este:

![Script instal ansible](/posts/instalacion-y-configuracion-de-ansible-en-linux/script-install-ansible.png)

Este script realiza las siguientes tareas:

* Instala requisitos básicos: vim, curl, git
* Instala repositorio epel y actualiza repos
* Instalación de ansible y dependencias
* Configuración de directorios de ansible

Ejecutamos el script para realizar la instalación:

```shell
sudo bash bin/bootstsrap.sh
```

El resultado lo podemos probar validando la existencia del binario ansible y los directorios:

![Ansible version](/posts/instalacion-y-configuracion-de-ansible-en-linux/ansible-version.png)

Ansible usa usa el protocolo SSH para establecer las conexiones con los nodos que vamos a administrar. En la máquina
node01 debemos generar un par de llaves RSA, usaremos el script **bin/generate-ssh-keys.sh**.

```shell
sudo vim bin/generate-ssh-keys.sh
```

El contenido del script es:

![Shell script ejemplo](/posts/instalacion-y-configuracion-de-ansible-en-linux/shell-script-ejemplo.png)

Ejecutamos el script generador de llaves ssh:

```shell
sudo bash bin/generate-ssh-keys.sh
```

Ahora copiamos la llave a la otra máquina usando el comando **ssh-copy-id**:

![SSH Copy id ansible](/posts/instalacion-y-configuracion-de-ansible-en-linux/ssh-copy-id-ansible.png)

## Configuración de Ansible

Ansible se configura a través de su archivo de configuración **/etc/ansible/ansible.cfg** y a través de parámetros en
tiempo de ejecución.

Editamos el archivo de configuración **/etc/ansible/ansible.cfg** y definimos la ruta al archivo de inventario.

```shell
sudo vim /etc/ansible/ansible.cfg
```

Cambiamos estos parámetros:

![Ansible config](/posts/instalacion-y-configuracion-de-ansible-en-linux/ansible-config.png)

Ahora vamos a configurar el inventario, es decir, vamos a agregar los nodos que vamos a administrar, editamos el
archivo del inventario y agregamos las siguientes líneas:

```shell
sudo vim /etc/ansible/inventory/hosts
```

![Ansible inventory](/posts/instalacion-y-configuracion-de-ansible-en-linux/ansible-inventory-all.png)

En este inventario hasta arriba definimos el bloque de variables globales **[all:vars]**, que aplican a todos los
hosts, aquí definimos la llave y el nombre de usuario para la conexión ssh hacía las otras máquinas que
administraremos, también definimos algunas otras variables que son especificas para algunos playbooks.

Después definimos el bloque de hosts general **[all]**, aquí definimos la lista de nodos que vamos a administrar,
básicamente usamos dos campos, uno par definir el nombre del host y otro para definir su dirección IP.

En este caso estamos agregando lo siguiente:

* nodo administrado: rpi
* Dirección IP del host SSH: 192.168.3.10

Ahora ejecutemos ansible en modo ad-hoc y hagamos uso del módulo ping:

![Ansible ping](/posts/instalacion-y-configuracion-de-ansible-en-linux/ansible-ping.png)

Ahora que ya hemos configurado ansible a través de su archivo de configuración y del inventario terminamos esta
sección y que hemos validado la conectividad vía SSH entre las dos máquina, en las siguientes publicaciones
mostraremos como creamos los diferentes roles para configuraciones generales del sistema, el servicio SSH, Docker
y Pi-hole.

El código de ejemplo lo pueden descargar de Gitlab en el siguiente URL:
[https://gitlab.com/jorge.medina/ansible-pihole](https://gitlab.com/jorge.medina/ansible-pihole).

El segundo articulo de la serie lo puedes encontrar
[acá](https://www.kronops.com.mx/blog/random-1/post/automatizando-el-despliegue-de-servidores-linux-con-ansible-10).
