+++
date = 2020-05-14
title = "Gestión de código Git"
description = "O como interactuar con tu repositorio"
authors = ["Luis Carrillo"]
categories = [
  "desarrollo",
  "git",
  "repositorio",
  "linux",
  "zsh",
]
+++

## Presentación

Aun era usuario de Windows cuando al fin desistí de usar software privativo (sobre todo impulsado por no pagar por el,
pero harto de buscar cracks), y buscando alternativas encontré formas de conseguir programas plenamente funcionales
sin necesariamente pagar por ellos. La mayoría venia en cómodos ejecutables, y entendía la necesidad de bajarlos
de fuentes confiables, pues recordemos que Troya cayó creyendo recibir un regalo. Había otros proyectos muy
prometedores que me dirigían a páginas con grandes listas de archivos acompañados (algunas veces) de crípticas
instrucciones, pero como dije al principio, me asomaba al umbral de ser un power-user, y eran días en que Windows XP
se aferraba como estándar. Intentaba bajar alguno de los archivos disponibles esperando que fuera "el Bueno": te
podrás imaginar como acabo aquello. Me hice alérgico a esas paginas en GitHub, Bitbucket y Gitlab que solo me
confundían mas de lo que me ayudaban, ademas me pedían clonarlos (cuando se dignaban a explicar como funcionaba su
software). Clonarlos... ahí fue cuando pensé "bueno, dejemos esto a los científicos".

Pero los caminos de la vida no son como yo pensaba, y de pronto me vi con la meta de aprender a codear. Gracias a
que obtuve asesoría de personas talentosas y con experiencia en ambientes reales, evite hacer malabares para manejar
los archivos de los programas que íbamos escribiendo, y de paso tuve un momento de epifanía al entender que esas
paginas que me había topado en otra época y que termine ignorando tenían una misión esencial: compartir no solo los
programas, si no el código que los hace funcionar. Ah, y se llamaban Repositorios. Boom.

Cayo un velo de mis ojos, y pude ver maravillas que solo sospechaba: Free software, Open Source, proyectos derivados,
personalización de programas que ya eran geniales ¡Incluso librerías con código ya listo para montar un sitio de
aspecto profesional con tecnologías modernas!. Y esto era solo el principio, fue cuestión de tiempo para enterarme
que en ellos se puede ver como están escritos sistemas operativos completos, y que, gracias a estas bibliotecas de
Alejandría, el omnipresente Linux ha evolucionado gracias a la colaboración de gente apasionada... y de gente
interesada, pues hasta los gigantes de la Informática, siempre guardianes de la propiedad intelectual, se han
abierto a colaborar e invertir en estos proyectos de comunidad. Pero ya me acelere mucho, así que déjame contarte
que son estos famosos y mágicos repositorios.

## El propósito

Pienso en un repositorio como en un directorio de archivos con esteroides. En realidad, puedes usar el modelo de
repositorio con otros fines que no sean guardar código fuente, pero es en esta actividad donde mas beneficios se
obtienen. Lo que hace especial a un directorio que alberga un repositorio, es que cada archivo que contiene esta
versionado.

Una manera fácil de entender para que y como versionar, es imaginándonos a un bibliotecario muy celoso, tan celoso
que conoce el contenido de cada linea de los libros que resguarda. El se ha vuelto así porque todo el material del
acervo es interdependiente, con referencias profundas entre ellos. Si alguno cambia, puede invalidar el contenido de
otro u otros libros. Es mas, es tan celoso que solo ofrece copias para la consulta en la sala de lectura.

Pero no es un retrogrado: Si un usuario modifica alguna palabra o frase de los libros y se compromete con los cambios
que realizó, el almacenara esa nueva versión, y en adelante el bibliotecario dará las copias de este libro con los
nuevos cambios. Claro, aun conserva el libro original en la bodega,  pero para la consulta habitual ofrecerá siempre
el libro mas actualizado. Está por supuesto en los derechos del usuario agregar nuevos libros para integrarlos con
los ya existentes.

Ahora supongamos que un usuario en particular tiene un proyecto grande que afecta varios de los libros de esta
biblioteca. Nuestro bibliotecario le propone no confundir a los demás usuarios con todos los cambios que seguramente
no podrá realizar al mismo tiempo, así que le ofrece trabajar en una nueva sala de consulta, con su propio juego de
copias. Si el proyecto es exitoso, los libros de esa nueva sala se fusionaran con la sala principal.

Pero quizá, el proyecto de este usuario tiene una visión distinta de los fundadores de la biblioteca, y sus planes
modificarían la intención original del proyecto, pero usarlo daría un gran impulso al nuevo al no tener que comenzar
desde cero. No hay problema: este bibliotecario le ayudara a levantar una biblioteca nueva para el, lo cual le dará
la oportunidad de llevar la evolución de su proyecto independientemente del original, de forma paralela, y con la
oportunidad de consultar o citar los libros del otro acervo que resuelvan problemas que ambos hayan encontrado.
Tutti contenti.

Ahora que conocemos las mecánicas de un repositorio, consideremos que estuvimos hablando de una biblioteca en una
universidad, a la cual solo tienen acceso los usuarios del campus. Pero ¿Y las personas que no pueden trabajar
siempre en la universidad? ¿Están limitadas a trabajar sobre las copias que pueden consultar ahí? Para nada.
Somos afortunados de tener a este poderoso y celoso guardian de los libros, porque es capaz de mandar a un clon
de sí mismo para trabajar en tu casa, con todo y su juego de copias. Este clon se mantiene comunicado con su
original en el campus, encargándose de mantener la integridad entre tu biblioteca local (y la de todo aquel que
tiene un clon en casa) y la central en la universidad. ¿Fascinante, cierto?.

## Git: El VCS más extendido

Ahora veamos como se realizan las solicitudes a este bibliotecario. El software mas popular que responde a la analogía
anterior se llama GIT. GIT forma parte de una clase de programas conocidos como VCS (Version Control Systems) que
permiten a individuos o grupos de trabajo progresar en sus proyectos de software de forma eficiente y disminuyendo
los conflictos (o generando mas, si no los utilizan de manera correcta). GIT fue originalmente escrito por Linus
Torvalds para evitar interactuar directamente con personas con las que colaboraba en distintos desarrollos, pero
principalmente en el Kernel de Linux, por el cual todos lo amamos, aunque el prefiera que no se lo demostremos.

Antes de entrar a mostrar el funcionamiento de GIT, usemos los elementos de la "biblioteca" anterior para conocer
sus conceptos mas importantes. La "biblioteca" es el directorio donde tenemos el repositorio. Las "salas de consulta"
son lo que en GIT se conoce como **ramas** (**branches**), los libros, evidentemente, son los archivos que pertenecen
a el proyecto, también llamados **tracked files**. Los "cambios con compromiso" que aportan los usuarios se conocen
como **commits**. Las "fusiones entre salas" las conoceremos como **merges**. El acto de hacer una nueva biblioteca
es lo que llamamos **forks**. La locación en la universidad representa un **repositorio central**, y a la oportunidad
de trabajar en casa **repositorio local**.

Para esta guía, tomaremos como escenario la consola de un sistema Ubuntu, pero aplica para cualquier entorno
_*nix like_, y he escuchado que en Windows es cada vez mas fácil contar con una terminal funcional con los mismos
principios, pero queda fuera de las intenciones de este articulo ahondar es esa cuestión. Para saber si tenemos
instalado y funcionando GIT en nuestro sistema, preguntemos que versión hay instalada:

```shell
$ git --version
git version 2.25.1
```

Si no se encuentra en tu sistema, los shell modernos te sugieren que comando usar para instalarlo, en el caso de
Ubuntu puedes usar aptitude para instalarle tecleando:

```shell
sudo apt install git
```

¿Como saber si mi directorio es un repositorio?

A nivel estructura, un directorio es un repositorio de GIT si contiene un subdirectorio de nombre **.git** con
datos de versionado en él. Permíteme mostrarte:

```shell
**my_ansible_project**
├── deploy-general-system.yml
├── **.git**
│   ├── **branches**
│   ├── COMMIT_EDITMSG
│   ├── config
│   ├── description
│   ├── HEAD
│   ├── **hooks**
│   ├── index
│   ├── i**nfo**
│   ├── **logs**
│   ├── **objects**
│   └── **refs**
├── localsystem.yml
├── README.md
└── **roles**
```

Si estas seguro que tu directorio es un repo y aun así no ves ninguno nombrado .git, intenta listar también los
directorios ocultos (en `nix` se asigna como oculto si su nombre empieza con un punto `.`). A nivel aplicación,
puedes preguntar a GIT la condición del repositorio usando el comando `status`, tecleando:

```shell
$ git status
On branch main
Your branch is up to date with 'origin/main'.

nothing to commit, working tree clean
```

Lo cual nos arroja información valiosísima. Primero nos indica en que rama estamos trabajando. En la siguiente
sección detalla el estado de nuestro repositorio local con respecto al repositorio central. El repositorio central
puede estar alojado en algún sitio en internet (Gitlab o Github, los mas populares) o en un servidor privado en tu
red o VPN.  Por último nos informa de la situación de los archivos con seguimiento (tracked files) y si hay archivos
sin incluir aun en el versionado (Untracked files).

Hay algunos shell que te permiten conocer el estado de tu repositorio en cuanto lo uses como directorio de trabajo
(o te muevas a el, como decimos coloquial mente) ahorrándote el usar un comando para saber que todo esta bien. Llevo
tiempo utilizando oh-my-zsh con un tema que me imprime el branch y el estado de mi repositorio, aquí te pego un
screenshot comparando un prompt normal en bash en la parte inferior y arriba zsh tuneado:

![Git zsh](/posts/gestion-de-codigo-git/git-example-1.png)

Si hay algún cambio en tu repositorio, zsh te reporta rápidamente si hay archivos modificados o si existen archivos
sin seguimiento.

![Git zsh alias](git-example-2.png)

Otra característica de zsh es que trae varios alias pre cargados, y muchos son para la interacción con GIT. Si eres
observador, te habrás percatado que en zsh solo introduje gst como comando para conocer el estado del repositorio,
el cual es un alias para git status. Créeme, estarás usando muchas veces los mismos comandos al aportar a algún
proyecto, y los alias te ahorraran mucho tiempo y harán mas fluida tu labor. Aun si decides seguir usando bash o
mantener sencillo tu prompt, te recomiendo mucho hacer algunos alias cortos para los comandos de GIT, y usar esa
filosofía para tus comandos mas concurrentes.

Actualización: Para promover una sociedad libre de prejuicios y conceptos con carga histórica negativa, actualmente
se usa el nombre main para designar a la rama principal de un repositorio, abandonando el uso de master.

Bien, hasta aquí esta entrega. En la próxima me gustaría platicarte sobre como usar los comandos para modificar tu
repositorio de GIT en un flujo de trabajo diseñado para evitar conflictos (GitLab Flow), y en posteriores entregas
como configurar tu terminal para usar powerfonts y temas de oh-my-zsh. ¡Gracias por leer!
