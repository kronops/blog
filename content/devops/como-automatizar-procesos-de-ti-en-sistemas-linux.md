+++
date = 2020-02-24
title = "Como automatizar procesos de TI en sistemas Linux"
subtitle = "Shell scripts y herramientas de configuración"
authors = ["Jorge Medina"]
categories = [
    "automatizacion",
    "configuration management",
    "linux",
]
+++

Todo empezó hace casi 10 años, entonces ya llevaba por ahí de 10 años usando sistemas GNU/Linux en mi vida, tanto
sistemas para uso personal, es decir, mi escritorio o mi laptop de casa o la del trabajo, aquí es donde empece a
ver la necesidad de tener un proceso repetible y reproducible para instalar y configurar mis propios equipos,
aunque en los sistemas de escritorios los instalo una o dos veces al año, los sistemas de servidores de red para
empresas son más recurrentes, a veces tengo que instalar desde un par hasta unas decenas al mes en mi trabajo
como administrador de sistemas (sysadmin).

Cuales son los procesos que se pueden automatizar? pueden ser tareas que comúnmente reconocemos como:

* Configurar redes físicas o virtuales.
* Configuración de almacenamiento local y en red.
* Aprovisionamiento de máquinas físicas o virtuales.
* Instalación de el Sistema operativo.
* Instalación del kit de herramientas generales de sistema.
* Instalación de servicios de bases de datos.
* Instalación de servicios de servidores web.
* Instalación de aplicaciones en diferentes lenguajes y frameworks.
* Instalación y configuración de directivas de seguridad o hardening.
* Instalación y configuración de herramientas de respaldos.
* Instalación y configuración de herramientas de monitoreo.

Saben cuanto tiempo toma instalar un servidor y dejarlo a punto? unos dicen **un par de horas** y otras veces el
resultado es **un par de semanas** o meses en el peor de los casos.

Aunque es cierto que la virtualización ha simplificado las tareas requeridas para aprovisionar servidores y de que
casi todas las distribuciones GNU/Linux tienen métodos para automatizar la instalación del sistema operativo, por
ejemplo las distribuciones basadas en Red hat usan [Kickstart](https://es.wikipedia.org/wiki/Kickstart_(Linux),
las distribuciones basadas en Debian tienen [Preseed](https://wiki.debian.org/DebianInstaller/Preseed), o las
basadas en SuSE tienen [AutoYast](https://doc.opensuse.org/projects/autoyast/), esto solo cubre poco menos de la
mitad de tareas que arriba describo.

Soy de los que se dicen flojos (*siempre busco un atajo*), no me gusta repetir mucho el trabajo arriba descrito,
como dicen por ahí, no aporta mucho valor. Por eso empece a escribir en shell scripts estas rutinas de instalación,
por eso empece a escribir guías de despliegue de las diferentes soluciones open source que tanto me gusta entregar
a mis clientes, pero esto no fue suficiente. En el mundo empresarial donde se tienen que manejar decenas y centenas
de servidores Linux, manejar tantos shell scripts se puede convertir en algo engorroso, difícil de mantener,
y ni que decir de mantener la documentación.

En el mundo empresarial conocí diferentes estilos de programación de shell scripts, de grandes maestros
administradores de sistemas y desarrolladores, el TRokHE, el Nito, el don Kino, el Christian, el Axel, el Rodrigo,
el Neto (net0bsd), todos ellos aportaron a mi gran interés de hacer las cosas más simples, entonces me presentan
[CFEngine](https://github.com/cfengine/core), una herramienta hecha para gestionar las configuraciones de los
sistemas Linux y Unix, con CFEngine se podía hacer esa lista grande de tareas que se hacen después de instalar el
sistema operativo, con esto se puede lograr tener consistencia en las configuraciones, tener procedimientos repetibles
y reproducibles. Con la virtualización basada en [Xen](https://xenbits.xen.org/), con los preseed y autoyast
podíamos automatizar la instalación del sistema operativo, y con CFEngine podíamos hacer los despliegues de
bases de datos, servidores web, aplicaciones y todo el conjunto de tareas post instalación.

Pero CFengine tenia una curva de aprendizaje muy grande, todavía buscaba algo más simple, y buscando en la red,
me encontré con este gran libro que tiene titulo
[Taste Test](https://valdhaus.co/books/taste-test-puppet-chef-salt-stack-ansible.html), en este libro comparan
diferentes métodos para automatizar las tareas, desde el uso convencional de shell scripts, hasta el uso de otras
herramientas de las que se conocen como [**configuration management**](https://en.wikipedia.org/wiki/Configuration_management),
**provisioning management** y otros títulos rimbombantes.

Este libro te lleva de la mano en el uso de las diferentes herramientas como Puppet, Chef, Ansible y Saltstack
para automatizar los diferentes procesos de TI que arriba se mencionan, es una buena guía para aprender de las
diferentes herramientas y elegir la que mejor se adapte a tu personalidad. Pueden bajar el capitulo de prueba
para convencerse [Shell Script Sample Chapter](https://valdhaus.co/books/taste-test-shell-script-sample-chapter.pdf).

Hasta aquí llegamos en esta entrega, en las próximas publicaciones escribiré más de los shell scripts y playbooks
con [Ansible](https://es.wikipedia.org/wiki/Ansible_(software)), si les pareció informativo el post compartan lo
con sus amigos :).
