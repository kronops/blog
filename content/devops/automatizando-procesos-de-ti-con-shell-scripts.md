+++
date = 2020-02-29
title = "Automatizando procesos de TI con shell scripts"
subtitle = "Introducción al shell scripting en Linux"
authors = ["Jorge Medina"]
categories = [
    "automatizacion",
    "git",
    "linux",
    "shell scripts",
]
+++

Que tal, en esta entrega voy a hablar sobre los shell scripts para sistemas GNU/linux. Empecemos entendiendo por qué
debemos aprender a programar shell scripts. Como dije antes, soy un flojo, y no me gusta repetir el trabajo mundano.
Todos los días ejecuto cientos de comandos en una o más terminales de mi propio equipo o de equipos remotos, por lo
tanto todos los días repito las mismas secuencias, otras veces las cambio al vuelo. Un shell script nos permite
escribir un dialogo predefinido, una secuencia de pasos que se deben repetir con cierta consistencia.

![Ejemplo shell script](/posts/automatizando-procesos-de-ti-con-shell-scripts/shell-script-example-1.png)

En el mundo de la administración de sistemas los shell scripts son muy usados para automatizar:

* Instalación de dependencias y paquetes de programas.
* Configuración de servicios y aplicaciones.
* Ejecución de servicios o rutinas batch.
* Actualización de programas.
* Mantenimientos como purgar logs.
* Reiniciar servicios.
* Respaldar aplicaciones o bases de datos.
* Generar reportes.

En GNU/Linux la mayoría de veces se usa el interprete de comandos [bash(1)](https://es.wikipedia.org/wiki/Bash),
también se tienen otros interpretes como **zsh** que se ha vuelto muy popular entre los programadores. En esta
sección me enfocaré más en bash, es importante que leas un manual básico de bash, puede ser:

```shell
man bash
```

Empezamos escribiendo un archivo en texto plano usando tu editor favorito, recuerda ponerle terminación `.sh`
a tu archivo, será más fácil identificar su propósito.

```shell
vim getgit.sh
```

La primer línea que siempre debemos escribir es el famoso _shebang_:

```shell
#!bin/bash
```

Después es bueno agregar unos breves comentarios: el mismo nombre del script, el autor, y una pequeña descripción
de lo que hace el programa.

```shell
# script: getgit.sh
# desc: Get latest updates from git repo.
# author: jmedina@kronops
```

Estos datos son sumamente importantes, y más si piensas contribuir con tu programa a una comunidad, equipo o para el futuro.

En lo personal trato de escribir en el script una sección para definir variables y otra con el código principal del script. Así qué pongo esto:

```shell
# vars
REPO_URL=https://gitlab.com/kronops/k-maji-orquesta.git
REPO_DIR=/home/jmedina/data/vcs

# main
echo "Get to $REPO_DIR"
cd $REPO_DIR
git clone $REPO_URL
```

Al final de todo script se recomienda escribir el código de salida:

```shell
exit 0
```

En este ejemplo puse una sección principal muy simple, pero tus scripts ejecutarán diferentes lógicas que van a
depender de los problemas que quieras resolver. Te comparto algunos manuales o HOWTOs que me han servido para
aprender a programar shell scripts desde lo básico hasta lo avanzado:

* [BASH Programming - Introduction HOW-TO](https://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html).
* [Bash Guide for Beginners](https://www.tldp.org/LDP/Bash-Beginners-Guide/html/)
* [Advanced Bash-Scripting Guide: An in-depth exploration of the art of shell scripting](https://www.tldp.org/LDP/abs/html/)
* [SuSE Administration Guide => Common Tasks => Bash and shell scripts](https://documentation.suse.com/sled/15-SP1/html/SLED-all/cha-adm-shell.html)

También les recomiendo mucho leer el capitulo de prueba del libro **Taste Test** para ver los casos de uso
prácticos, aquí el enlace: [PDF](https://valdhaus.co/books/taste-test-shell-script-sample-chapter.pdf).

No olvides mantener tus scripts en un lugar seguro pero accesible, en lo personal tengo scripts de uso personal
en `/home/jmedina/bin`, este es un directorio que agrego al **PATH** de mi shell bash. Los scripts que son para
uso de general en el sistema, los pongo en `/usr/local/bin`, y los scripts que son específicos para la
administración de sistemas los pongo en `/usr/local/sbin`.

Para mantener el código de estos scripts he venido usando (desde hace más de 12 años) sistemas de control de
versiones o **vcs**, desde el tradicional [CVS](https://es.wikipedia.org/wiki/CVS), seguido de su sucesor
[Subversion](https://es.wikipedia.org/wiki/Subversion_(software)), hasta llegar al reconocidísimo
[GIT](https://es.wikipedia.org/wiki/Git). En lo personal recomiendo ampliamente tener versionados sus scripts.
Pueden usar [GitLab](https://about.gitlab.com/) o [GitHub](https://github.com/) para crear sus repositorios
públicos o privados, y también pueden tener repositorios git privados y cifrados con [Keybase](https://keybase.io/).

En la escritura de shell scripts hay muchos estilos. Algunos programadores preferimos usar muchos comentarios y
líneas en blanco para ordenar el código, y otros prefieren eliminar los comentarios, y líneas en blanco y mejor
hacer el código legible y funcional. Aquí lo importante es tomar lo mejor de todos estos maestros de la programación
y hacer lo que funcione en tu caso. Al aprender shell scripting es importante que no aprendas solo las estructuras
de control básicas, también debes aprender como parametrizar tus scripts. Recuerda que es muy posible que ese
script lo vuelvas a necesitar en otros ambientes, con otros clientes, otros proyectos. También aprende a hacer
control de errores, y como plus ponerle colores :).

En los próximos artículos estaremos compartiendo algunos shell scripts que usamos en el día a día de la
administración de sistemas, estén atentos.

Por ahora es todo, espero que esta edición les haya sido de utilidad. Si les gusta compartan el articulo con
sus amigos y si tienen dudas pueden dejar sus comentarios. Saludos!.
