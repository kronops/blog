+++
date = 2020-01-27
title = "Acerca de"
authors = ["Jorge Medina"]
categories = [
    "automatizacion",
    "infraestructura",
    "operaciones",
]
+++

Aquí compartimos nuestra experiencia en el desarrollo de software, la administración de sistemas y otras prácticas
que hemos aprendido durante nuestra carrera profesional y que nos han ayudado hacer el trabajo más simple y divertido.
