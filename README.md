# Blog de KronOps

## Requisitos

Usar editor de textos con plugin para `editorconfig`, por ejemplo para `vim` existe el
repo [editorconfig-vim](https://github.com/editorconfig/editorconfig-vim).

Instala el las herramientas del entorno de desarrollo Node.js:

```shell
curl -SLO https://deb.nodesource.com/nsolid_setup_deb.sh
chmod 500 nsolid_setup_deb.sh
./nsolid_setup_deb.sh 20
apt-get install nodejs -y
```

Instala [markdownlint-cli2](https://github.com/DavidAnson/markdownlint-cli2) para hacer pruebas de sintaxis en los archivos markdown:

```shell
npm install markdownlint-cli2 --global
```

## Pruebas

Siempre haz pruebas de la nueva documentación:

```shell
markdownlint-cli2 content/posts/my-new-guide.md
```

Se recomienda que no hagas commit hasta que hayas resuelto todos los issues.
